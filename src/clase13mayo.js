/* eslint-disable linebreak-style */
/* eslint-disable import/order */
const http = require('http');
// var log = require("../modules/my-log");
const url = require('url');
const querystring = require('querystring');
const { info, error } = require('../modules/my-log');
const { countries } = require('countries-list');

const server1 = http.createServer((req, res) => {
  const parsed = url.parse(req.url);
  // console.log("parsed: ", parsed);
  const { pathname } = parsed;
  const query = querystring.parse(parsed.query);
  console.log('query: ', query);

  if (pathname === '/') {
    res.writeHead(200, { 'Content-Type': 'text/html' });
    res.write('<html><body><h1>HOME!!!</h1></body></html>');
    res.end();
  } else if (pathname === '/country') {
    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.write(JSON.stringify(countries[query.code]));
    res.end();
  } else if (pathname === '/exit') {
    res.writeHead(200, { 'Content-Type': 'text/html' });
    res.write('<html><body><h1>BYE</h1></body></html>');
    res.end();
  } else if (pathname === '/info') {
    const result = info(req.url);
    res.writeHead(200, { 'Content-Type': 'text/html' });
    res.write(result);
    res.end();
  } else if (pathname === '/error') {
    const result = error(req.url);
    res.writeHead(200, { 'Content-Type': 'text/html' });
    res.write(result);
    res.end();
  } else {
    res.writeHead(404, { 'Content-Type': 'text/html' });
    res.write('<html><body><h1>not found</h1></body></html>');
    res.end();
  }
});

server1.listen(4000);
console.log('Running on 4000');
