/* eslint-disable linebreak-style */
// const log = require('../modules/my-log'); //.. si esta fuera del mismo directorio
// const { error, info } = require('../modules/my-log');
const chalk = require('chalk');
const { image } = require('image-js');
const express = require('express');
const { countries, languages } = require('countries-list');

const app = express();

app.listen(4000, () => {
  console.log('Running on 4000 port');
});

app.get('/', (reques, response) => {
  response.status(200);
  response.send('<html><body><h1>Estoy en home</h1></body></html>');
});

app.get('/info', (reques, response) => {
  response.send('<html><body><h1>Informacion</h1></body></html>'); // si no coloca status asume que esta correcta
});

app.get('/lenguages/:lang', (reques, response) => {
  console.log('reques.parans', reques.params);
  response.json(languages[reques.params.lang]);
});

app.get('/chalk', (reques, response) => {
  execute().catch(console.error);
  async function execute() {
    const img = await image.load(
      'D:/documentos/Programación Avanzada 2/proyectos nodejs/imagenes/ejm.png',
    );
    const grey = img;
    grey.grey();
    grey.resize({ width: 200 });
    grey.rotate(30);
    grey.save('editado.png');
    return grey.save('editado.png');
  }
  response.send(
    "<html><body><img src='D:/documentos/Programación Avanzada 2/proyectos nodejs/imagenes/editado.png' /></body></html>",
  );
  console.log(chalk.blue('******IMAGEN CREADA*******'));
});

app.get('*', (reques, response) => {
  response
    .status(404)
    .send('<html><body><h1>Pagina no encontrada</h1></body></html>');
});

/*
var http = require("http"); //modulo de nodejs para menejo de http

var server = http.createServer(function (request, response){
    //reques -> lo que llega de un formumulario con submin
    //response -> lo que se envia al servidor
    if (request.url === "/")
    {
        response.writeHead(200,{"Content-Type":"text/html"}); //200 esta bien y se responde con un html
        response.write("<html><body><h1>Estoy en home</h1></body></html>");
        response.end();//envia la respuesta
    }
    else if(request.url === "/exit")
    {
        response.writeHead(200,{"Content-Type":"text/html"}); //200 esta bien y se responde con un html
        response.write("<html><body><h1>bye</h1></body></html>");
        response.end();//envia la respuesta
    }
    else if(request.url === "/info")
    {
        //var result = log.info(request.url);
        var result = info(request.url);
        response.writeHead(200,{'Content-Type':'text/html'}); //200 esta bien y se responde con un html
        response.write(result);
        response.end();//envia la respuesta
    }
    else if(request.url === "/chalk")
    {
        //response.writeHead(200,{'Content-Type':'aplication/json'})
        response.writeHead(200,{'Content-Type':"text/html"})
        //response.write(chalk.blue('Hello world'));
        //response.write(JSON.stringify(chalk.blue('hola mundo')));
        execute().catch(console.error);
        async function execute() {
            let img = await image.load('../imagenes/ejm.png');
            let grey = img
            grey.grey()
            grey.resize({ width: 200 })
            grey.rotate(30);
            grey.save('editado.png')
            return grey.save('editado.png');
        }
        response.write("<html><body><img src='D:\documentos\Programación Avanzada 2\proyectos nodejs\imagenes\editado.png' /></body></html>");
        console.log(chalk.blue('imagen creada'));
        response.end()
    }
    else
    {
        var result = error(request.url);
        response.writeHead(404,{'Content-Type':'text/html'}); //200 esta bien y se responde con un html
        response.write(result);
        response.end();//envia la respuesta
    }


});
server.listen(4000);
console.log("sever is running on 4000")
//fs // acceder al sistemas de archuvis crear,editar... (file system) todo del lado servidor */
