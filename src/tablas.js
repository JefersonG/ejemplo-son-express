var http = require("http");
var url  = require("url");

var server = http.createServer(function (request, response){
    var numero=url.parse(request.url,true).query.num;
    if (request.url === "/")
    {
        response.writeHead(200,{'Content-Type':"text/html"}); //200 esta bien y se responde con un html
        response.write("<html><body><h1>Tabla de multiplicar</h1></body></html>");
        response.end();//envia la respuesta 
    }
    else if(request.url === "/exit")
    {
        response.writeHead(200,{'Content-Type':"text/html"}); //200 esta bien y se responde con un html
        response.write("<html><body><h1>bye</h1></body></html>");
        response.end();//envia la respuesta 
    }
    else if(request.url === "/?num="+numero && parseInt(numero))
    {
        response.writeHead(200,{'Content-Type':"text"}); //200 esta bien y se responde con un html
        response.write("Tabla de multiplicar del "+ numero + "\n");
        for (let i = 1; i < 13; i++) 
        {
            response.write(numero + " * " + i + " = " + parseInt(numero)*i + "\n");
        }
        response.end();//envia la respuesta 
    }
    else
    {
        response.writeHead(404,{'Content-Type':"text/html"}); //200 esta bien y se responde con un html
        response.write("<html><body><h1>Pagina no encontrada</h1></body></html>");
        response.end();//envia la respuesta 
    }
    

});
server.listen(4000);
console.log("sever is running on 4000")